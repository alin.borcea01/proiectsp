import ro.uvt.models.Element;
import ro.uvt.models.Image;
import ro.uvt.models.Paragraph;
import ro.uvt.models.Section;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class SectionTest {
    @Test
    public void givenSection_whenChildHasNoParent_thenAddChild() {
        Section section = new Section("test");
        Element child = new Image("");

        section.add(child);
        assert(child == section.get(0));
    }

    @Test
    public void givenSection_whenChildHasNoParentAndSameSection_thenThrowException() {
        Section section = new Section("test");
        Element child = new Image("");

        section.add(child);
        assertThrows(UnsupportedOperationException.class, () -> section.add(child));
    }

    @Test
    public void test3() {
        Section section = new Section("test");

        assertThrows(UnsupportedOperationException.class, () -> section.add(section));
    }

    @Test
    public void test4() {
        Element e = new Image("test");

        assertThrows(UnsupportedOperationException.class, () -> e.add(new Paragraph("")));
        assertThrows(UnsupportedOperationException.class, () -> e.remove(new Paragraph("")));
        assertThrows(UnsupportedOperationException.class, () -> e.get(0));
    }

    @Test
    public void test5() {
        Section section = new Section("test");

        assertThrows(UnsupportedOperationException.class, () -> section.add(section));
    }

    @Test
    public void test6() {
        Element e = new Paragraph("test");

        assertThrows(UnsupportedOperationException.class, () -> e.add(new Paragraph("")));
        assertThrows(UnsupportedOperationException.class, () -> e.remove(new Paragraph("")));
        assertThrows(UnsupportedOperationException.class, () -> e.get(0));
    }
}
