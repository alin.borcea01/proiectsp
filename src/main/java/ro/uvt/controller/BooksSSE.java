package ro.uvt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import ro.uvt.models.Book;
import ro.uvt.models.SseObserver;
import ro.uvt.services.ConcreteSubject;

@RestController
@RequestMapping(path = "/books-sse")
public class BooksSSE {
    private final ConcreteSubject allBooksSubject;

    @Autowired
    public BooksSSE(ConcreteSubject allBooksSubject) {
        this.allBooksSubject = allBooksSubject;
    }

    @GetMapping
    public ResponseBodyEmitter getBooksSse() {
        final SseEmitter emitter = new SseEmitter(0L);
        allBooksSubject.attach(new SseObserver(emitter));
        return emitter;
    }
}
