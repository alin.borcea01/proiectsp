package ro.uvt.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.uvt.models.Book;

import java.util.ArrayList;

@RestController
@RequestMapping(path = "/hello")
public class HelloController {


    @GetMapping
    public String getHello() {
        return "Hello World!";
    }


}
