package ro.uvt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.uvt.models.Book;
import ro.uvt.services.ConcreteSubject;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/books")
public class BooksController {
    private List<Book> books = new ArrayList<>();
    private final ConcreteSubject allBooksSubject;

    @Autowired
    public BooksController(ConcreteSubject allBooksSubject) {
        this.allBooksSubject = allBooksSubject;
    }

    @GetMapping
    public String getBooks() {
        return "These are the books!?";
    }

//    @PostMapping
//    public void post(Book book) {
//        books.add(book);
//    }

    @PostMapping
    public String newBook(/*@RequestBody String newBookRequest*/) {
        Book book = createBook();
        books.add(book);    //book = booksRepository.save(book);
        allBooksSubject.add(book);
        return "Book saved [" + book.getId() + "] " + book.getTitle();
    }

    Book createBook() {
        return new Book();
    }

}
