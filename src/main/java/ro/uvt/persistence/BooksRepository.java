package ro.uvt.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.uvt.models.Book;

public interface BooksRepository extends JpaRepository<Book, Integer> {
}
