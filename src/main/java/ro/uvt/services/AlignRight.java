package ro.uvt.services;

import ro.uvt.models.Paragraph;

import javax.naming.Context;

public class AlignRight implements AlignStrategy {
    private static final Alignment alignment = Alignment.right;

    @Override
    public void render(Paragraph p, Context c) {
        p.setText(p.getText() + "*******");
    }
}
