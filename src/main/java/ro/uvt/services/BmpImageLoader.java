package ro.uvt.services;

import ro.uvt.models.ImageContent;

import java.awt.*;

public class BmpImageLoader implements ImageLoader {
    @Override
    public ImageContent load(String id) {
        return new ImageContent("Id.bmp", new Dimension(id.length(), id.length()));
    }
}
