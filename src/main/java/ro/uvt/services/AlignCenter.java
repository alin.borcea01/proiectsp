package ro.uvt.services;

import ro.uvt.models.Paragraph;

import javax.naming.Context;

public class AlignCenter implements AlignStrategy {
    private static final Alignment alignment = Alignment.center;

    @Override
    public void render(Paragraph p, Context c) {
        p.setText("***" + p.getText() + "***");
    }
}
