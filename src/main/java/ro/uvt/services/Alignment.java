package ro.uvt.services;

public enum Alignment {
    left,
    center,
    right,
}
