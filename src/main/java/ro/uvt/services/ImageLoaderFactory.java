package ro.uvt.services;

import ro.uvt.models.ImageContent;

public class ImageLoaderFactory {
    public static ImageContent create(String id) {
        ImageLoader loader;

        switch (id) {
            case "bmp":
                loader = new BmpImageLoader();
                break;
            case "jpg":
                loader = new JPGImageLoader();
                break;
            default: return null;
        }

        return loader.load(id);
    }
}
