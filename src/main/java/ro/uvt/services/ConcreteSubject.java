package ro.uvt.services;

import org.springframework.stereotype.Component;
import ro.uvt.models.Book;
import ro.uvt.models.SseObserver;

import java.util.ArrayList;
import java.util.Collection;


@Component
public class ConcreteSubject {
    private final Collection<SseObserver> observers = new ArrayList<>();

    public void attach(SseObserver sseObserver) {
        observers.add(sseObserver);
    }

    public void add(Book book) {
        observers.forEach(x -> x.update(book));
    }
}
