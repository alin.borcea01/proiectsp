package ro.uvt.models;

import ro.uvt.services.ImageLoaderFactory;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.awt.*;
import java.util.concurrent.TimeUnit;

@Entity
public class Image implements Element, Picture, Visitee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String url;
    private ImageContent content;

    public Image() {}

    public Image(String url) {
        this.url = url;
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        content = ImageLoaderFactory.create(url);
    }

    @Override
    public void print() {
        System.out.println("Image with name: " + url + "\n");
    }

    @Override
    public String url() {
        return url + " " + (content != null ? content : "");
    }

    @Override
    public Dimension dim() {
        return content.getDim();
    }

    @Override
    public ImageContent content() {
        return content;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitImage(this);
    }
}
