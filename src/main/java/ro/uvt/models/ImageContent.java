package ro.uvt.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.awt.*;

@Entity
public class ImageContent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String content;
    private Dimension dim;

    public ImageContent() {}

    public ImageContent(String content, Dimension dim) {
        this.content = content;
        this.dim = dim;
    }

    public String getContent() {
        return content;
    }

    public Dimension getDim() {
        return dim;
    }

    @Override
    public String toString() {
        return content + " " + dim;
    }
}
