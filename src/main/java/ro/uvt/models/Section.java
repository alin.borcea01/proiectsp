package ro.uvt.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Section implements Element, Visitee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected String title;
    private List<Element> elements;

    public Section() {}

    public Section(String title) {
        this.title = title;
        elements = new ArrayList<>();
    }

    @Override
    public void print() {
        System.out.println(title);
        for (Element e : elements) {
            e.print();
        }
    }

    @Override
    public void add(Element e) {
        if (e == this || e.hasParent()) {
            throw new UnsupportedOperationException();
        }

        if (elements.contains(e)) {
            throw new UnsupportedOperationException();
        }
        elements.add(e);
        e.setParent(this);
    }

    @Override
    public void remove(Element e) {
        elements.remove(e);
    }

    @Override
    public Element get(int index) {
        return elements.get(index);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitSection(this);
    }
}
