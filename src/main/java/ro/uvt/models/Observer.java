package ro.uvt.models;

public interface Observer {
    void update(Book book);
}
