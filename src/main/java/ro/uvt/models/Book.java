package ro.uvt.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Book extends Section {
    private Observer observer;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private List<Author> authors;

    public Book() {
        super();
    }

    public Book(String title) {
        super(title);
        authors = new ArrayList<>();
    }

    public void addAuthor(Author author) {
        authors.add(author);
    }

    public void addContent(Element e) {
        super.add(e);
        observer.update(this);
    }

    public void registerObserver(Observer observer) {
        this.observer = observer;
    }

    @Override
    public void print() {
        System.out.println("Book: " + super.title);
        System.out.println("Authors\n");
        for (Author a : authors) {
            a.print();
        }
        super.print();
    }

    public String getId() {
        return "0";
    }

    public String getTitle() {
        return this.title;
    }
}
