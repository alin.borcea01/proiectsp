package ro.uvt.models;

import javax.persistence.Entity;

@Entity
public class Table implements Element, Visitee {
    private String something;

    public Table() {}

    public Table(String something) {
        this.something = something;
    }

    @Override
    public void print() {
        System.out.println("Table with title: " + something + "\n");
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitTable(this);
    }
}
