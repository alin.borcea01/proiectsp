package ro.uvt.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TableOfContents implements Element, Visitee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String something;

    public TableOfContents() {}

    public TableOfContents(String something) {
        this.something = something;
    }

    @Override
    public void print() {
        System.out.println("TableOfContents: " + something + "\n");
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitTableOfContents(this);
    }

}
