package ro.uvt.models;

public interface Element {
    void print();

    default boolean hasParent() {
        return false;
    }

    default void setParent(Element e) {}

    default void add(Element e) {
        throw new UnsupportedOperationException();
    }

    default void remove(Element e) {
        throw new UnsupportedOperationException();
    }

    default Element get(int index) {
        throw new UnsupportedOperationException();
    }
}
