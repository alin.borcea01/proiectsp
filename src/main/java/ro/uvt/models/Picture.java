package ro.uvt.models;

import java.awt.*;

public interface Picture {
    String url();
    Dimension dim();
    ImageContent content();
}
