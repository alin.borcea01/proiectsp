package ro.uvt.models;

import ro.uvt.services.AlignStrategy;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Paragraph implements Element, Visitee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String text;
    private AlignStrategy alignment;

    public Paragraph() {}

    public Paragraph(String paragraph) {
        this.text = paragraph;
    }

    public void setAlignStrategy(AlignStrategy align) {
        alignment = align;
        align.render(this, null);
    }

    public void setText(String text) {
        this.text = text;
    }
    public String getText() {
        return text;
    }

    @Override
    public void print() {
        System.out.println("Paragraph: " + text + "\n");
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitParagraph(this);
    }
}
