package ro.uvt;

import ro.uvt.models.*;

public class RenderContentVisitor implements Visitor {
    @Override
    public void visitBook(Book book) {
        System.out.println(book);
    }

    @Override
    public void visitSection(Section section) {
        System.out.println(section);
    }

    @Override
    public void visitTableOfContents(TableOfContents tableOfContents) {
        System.out.println(tableOfContents);
    }

    @Override
    public void visitParagraph(Paragraph paragraph) {
        System.out.println(paragraph);
    }

    @Override
    public void visitImageProxy(ImageProxy imageProxy) {
        System.out.println(imageProxy);
    }

    @Override
    public void visitImage(Image image) {
        System.out.println(image);
    }

    @Override
    public void visitTable(Table table) {
        System.out.println(table);
    }
}
